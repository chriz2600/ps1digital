# PS1Digital

Before submitting an issue, please check [Issues](https://gitlab.com/chriz2600/ps1digital/-/issues) to see, if the issue was already submitted.

## Documentation / Guides

- [Installer's Access Point Mode](https://gitlab.com/chriz2600/xhdmi-tools/-/blob/master/Installers.md#step-1)

- [Advanced Video Settings](https://psx.i74.de/avs/)

- [Main Configuration File](https://psx.i74.de/config/)

- [Per Game Database Settings Details](https://psx.i74.de/configdb/)

- [Lid/Door Control Instructions](https://psx.i74.de/lidcontrol/)

- [Manual Flash Instructions](https://psx.i74.de/man-flash/)

- [OSD Menu Settings](https://psx.i74.de/osdsettings/)

- [Per Game Settings Migration (from a version < v1.5.7)](https://psx.i74.de/pgs-oom/)

- [PS1 Signal Test Software)](https://psx.i74.de/sigtest/)

## Firmware Changelogs

- [`master`](https://firmware.i74.de/ps1/master/changelog.html)

- [`develop`](https://firmware.i74.de/ps1/develop/changelog.html)

- [`experimental`](https://firmware.i74.de/ps1/experimental/changelog.html)
